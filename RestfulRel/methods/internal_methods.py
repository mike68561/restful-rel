import time
import flask
import json

class InternalMethods():
    def __init__(self):
        """
        Instantiate
        """
        pass

    def db_connect(self, db, user, pass_word, mode, model=False):
        (db_version, db_value, db_request) = db
        (user_type, user_value, user_request) = user
        (pass_type, pass_value, pass_request) = pass_word
        (mode_type, mode_value, mode_request) = mode

        if model:
            (model_type, model_value, model_request) = model
            conect_address = "connectTo '" + db_value.strip() + "' '" + user_value.strip() + "' '" + pass_value.strip() + "' '" + mode_value + "' '" + model_value + "'"
        
        else:
            conect_address = "connectTo '" + db_value.strip() + "' '" + user_value.strip() + "' '" + pass_value.strip() + "' '" + mode_value + "'"

        user_conn = eval(conect_address)
        return user_conn

    def rest_call(self, connection, action, path, headers):
        """
        This function provides REST call functionality and provides JSON object for curl/browser requests.
        """
        approved_actions = {'native': True}
        action = action.lower()

        if action not in approved_actions:
            return 'Invalid DB operation'
        #print headers
        params = dict()
        for (k, v) in flask.request.args.iteritems():
            params[k.lower()] = v
            exec('%s = v' % k)
        
        restricted_keys = ['mode', 'connection', 'accept-encoding', 'pass', 'content-type', 'cache-control', 'user-agent', 'host', 'db', 'user', 'accept', 'accept-language', 'content-length']

        for (k, v) in headers.iteritems():
            key = str(k).lower()
            if key not in restricted_keys:
                print key
                #print v
                exec('%s = v' % key)
        
        statement = "SQL on connection %s" % params['query']
        #print statment
        print params
        #print var1
        #results = SQL on connection """ """params['query']
        results = eval(statement)
        print results
        json_results = json.dumps(results)

        return json_results

